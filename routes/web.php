<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(rand(0,1))
    {
        return view('welcome');
    }
    else
    {
        return response("Internal server eror.\ndear sir\nplease refresh many times until it work")->setStatusCode(500);;
    }
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
