@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

<table class="table">
  <tbody>
                    @foreach ($subjects as $id => $name_grade)
                    <tr>
                        @foreach ($name_grade as $name => $grade)
                            <td>
                            {{ $name }}
                            </td> <td>
                            {{ $grade }}
                            </td>
                        @endforeach
                    </tr>
                    @endforeach
 </tbody>
</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
