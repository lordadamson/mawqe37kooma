<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\User;
use App\Subject;

class usersSeeder extends Seeder
{

    private function insert_subject($seat_id, $subject_id, $grade)
    {
        if($grade == "")
        {
            echo "no grade!";
            return;
        }

        $user = User::where('seat_id', $seat_id)->first();
        $subject = Subject::find($subject_id);

        $user->subjects()->attach([$subject->id => ['grade'=> $grade]]);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $row = 1;
        $first = true;
        if (($handle = fopen("grades.csv", "r")) !== FALSE)
        {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
            {
                // skipping the fields row
                if($first)
                {
                    $first = false;
                    continue;
                }

                $num = count($data);

                if($num != 12)
                {
                    echo $num . " columns";
                    echo "bad csv file.";
                    fclose($handle);
                    die();
                }

                $row++;
                
                $first_name = explode(" ", $data[1]);
                $first_name = $first_name[0];

                DB::table('users')->insert([
                    'seat_id' => $data[0],
                    'name' => $data[1],
                    'year' => 4,
                    'email' => $first_name . $data[0] . '@cis.asu.edu.eg',
                    'password' => Hash::make("123456"),
                ]);

                echo $first_name . $data[0] . "@cis.asu.edu.eg\r\n";

                $this->insert_subject($data[0], 1, $data[2]);
                $this->insert_subject($data[0], 2, $data[3]);
                $this->insert_subject($data[0], 3, $data[4]);
                $this->insert_subject($data[0], 4, $data[5]);
                $this->insert_subject($data[0], 5, $data[6]);
                $this->insert_subject($data[0], 6, $data[7]);
                $this->insert_subject($data[0], 7, $data[8]);
                $this->insert_subject($data[0], 8, $data[9]);
                $this->insert_subject($data[0], 9, $data[10]);
                $this->insert_subject($data[0], 10, $data[11]);
            }

            fclose($handle);
        }
    }
}
