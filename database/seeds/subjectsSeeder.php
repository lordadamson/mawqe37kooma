<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class subjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('subjects')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('subjects')->insert([
            'name' => "Astronomy",
        ]);

        DB::table('subjects')->insert([
            'name' => "Defence Against the Dark Arts",
        ]);

        DB::table('subjects')->insert([
            'name' => "Flying",
        ]);

        DB::table('subjects')->insert([
            'name' => "Herbology",
        ]);

        DB::table('subjects')->insert([
            'name' => "History of Magic",
        ]);

        DB::table('subjects')->insert([
            'name' => "Charms",
        ]);

        DB::table('subjects')->insert([
            'name' => "Potions",
        ]);

        DB::table('subjects')->insert([
            'name' => "Transfiguration",
        ]);

        DB::table('subjects')->insert([
            'name' => "Arithmancy",
        ]);

        DB::table('subjects')->insert([
            'name' => "Divination",
        ]);
    }
}
